

const test1 = {
    "name": "Demo Store Tests",
    "steps": [{
            "action": "goto",
            "value": "https://enhancedecommerce.appspot.com/"
        },
        {
            "action": "test",
            "test": {
                "name": "Product on homepage",
                "description": "Check whether the first product's ID on the homepage matches the general format for product ID's",
                "for": "GoogleAnalytics",
                "type": "requestMatchRegex",
                "match": {
                    "key": "il1pi1id",
                    "value": "[a-z0-9]{5}"
                },
                "options": {
                    "matchAnyRequest": true
                }
            }
        },
        {
            "action": "click",
            "element": ".thumbnail a.itemLink"
        },
        {
            "action": "wait",
            "value": 1000
        },
        {
            "action": "click",
            "element": "#addToCart"
        },
        {
            "action": "test",
            "test": {
                "name": "Google Analytics Add to Cart event",
                "description": "Clicking add to cart on product detail page",
                "for": "GoogleAnalytics",
                "type": "requestMatchRegex",
                "match": {
                    "key": "ea",
                    "value": "add_to_cart"
                },
                "options": {
                    "matchAnyRequest": true
                }
            }
        }
    ]
};

const test2 = {
    name: "Demo Store Tests: Typing",
    steps: [{
            action: "goto",
            value: "https://enhancedecommerce.appspot.com/checkout"
        },
        {
            action: "click",
            element: "#start-customerInfo"
        },
        {
            action: "type",
            element: "#first_name",
            value: "Jason",
            clear: true
        },
        {
            action: "type",
            element: "#last_name",
            value: "Bourne",
            clear: true
        }
    ]

};