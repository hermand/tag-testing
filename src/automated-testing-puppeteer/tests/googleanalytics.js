const gaTests = {
    homepage: [
        {
            "action": "test",
            "test": {
                "name": "Google Analytics Page View",
                "description": "Page view fired on homepage load",
                "for": "GoogleAnalytics",
                "type": "requestMatchRegex",
                "match": {
                    "key": "t",
                    "value": "pageview",
                },
                "options": {
                    "matchAnyRequest": true,
                },
            },
        },
        {
            "action": "test",
            "test": {
                "name": "Google Analytics Page View event",
                "description": "Page view event fired on homepage load",
                "for": "GoogleAnalytics",
                "type": "requestMatchRegex",
                "match": {
                    "key": "ec",
                    "value": "pageview",
                },
                "options": {
                    "matchAnyRequest": true,
                },
            },
        },
        // {
        //     "action": "test",
        //     "test": {
        //         "name": "Google Analytics Add to Cart event",
        //         "description": "Clicking add to cart on product detail page",
        //         "for": "GoogleAnalytics",
        //         "type": "requestMatchRegex",
        //         "match": {
        //             "key": "ea",
        //             "value": "add_to_cart",
        //         },
        //         "options": {
        //             "matchAnyRequest": true,
        //         },
        //     }
        // },
    ],
};

module.exports = gaTests;