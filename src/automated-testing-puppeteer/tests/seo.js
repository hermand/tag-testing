const common = [
    {
        "action": "test",
        "test": {
            "name": "SEO - Title present",
            "description": "Page <title> tag present",
            "type": "matchSEOElement",
            element: {
                tag: 'title',
            }
        }
    },
    {
        "action": "test",
        "test": {
            "name": "SEO - Description present",
            "description": "Page <meta description> tag present",
            "type": "matchSEOElement",
            element: {
                tag: 'meta',
                attribute: 'name',
                value: 'description',
            }
        }
    },
    {
        "action": "test",
        "test": {
            "name": "SEO - Keywords present",
            "description": "Page <meta keywords> tag present",
            "type": "matchSEOElement",
            element: {
                tag: 'meta',
                attribute: 'name',
                value: 'keywords',
            }
        }
    },
    {
        "action": "test",
        "test": {
            "name": "SEO - Canonical present",
            "description": "Page canonical tag present",
            "type": "matchSEOElement",
            element: {
                tag: 'link',
                attribute: 'rel',
                value: 'canonical',
            }
        }
    },
    {
        "action": "test",
        "test": {
            "name": "SEO - Schema tag present",
            "description": "Page source contains schema tags",
            "type": "matchSEOElement",
            element: {
                tag: "script",
                attribute: 'type', // Need to add an ID for this element
                value: "application/ld+json"
            }
        },
    },
    {
        "action": "test",
        "test": {
            "name": "SEO - H1 tag present",
            "description": "Page source contains H1 tags",
            "type": "matchSEOElement",
            element: {
                tag: "h1",
            }
        },
    },
];

const seoTests = {
    homepage: [
        ...common,
        {
            "action": "test",
            "test": {
                "name": "SEO - Desktop TopNav Menu Item present",
                "description": "Page source contains menu item component",
                "type": "matchSEOElement",
                element: {
                    tag: 'ul',
                    attribute: 'data-component',
                    value: 'desktopTopNavMenuItem',
                }
            }
        },
        // {
        //     "action": "test",
        //     "test": {
        //         "name": "SEO - HD/WF/SY Hero present",
        //         "description": "Page source contains hero component",
        //         "type": "matchSEOElement",
        //         element: {
        //             tag: 'div',
        //             attribute: 'data-component',
        //             value: 'hero',
        //         }
        //     }
        // },
    ],
    category: [
        ...common,
    ],
    product: [
        ...common,
    ],
    content: [
        ...common,
    ],
};

module.exports = seoTests;