module.exports = {
    gaTests: require('./googleanalytics'),
    dlTests: require('./datalayer'),
    seoTests: require('./seo'),
};