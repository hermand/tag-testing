const dlTests = {
    homepage: [
        {
            "action": "test",
            "test": {
                "name": "Data layer - pageType",
                "description": "Data layer contains pageType for homepage",
                "type": "matchDataLayerKeyValue",
                "key": "pageType",
                "value": "homepage",
            }
        },
        {
            "action": "test",
            "test": {
                "name": "Data layer - pageName",
                "description": "Data layer contains pageName for homepage",
                "type": "matchDataLayerKeyValue",
                "key": "pageName",
                "value": "HomePage",
            }
        },
    ],
};

module.exports = dlTests;