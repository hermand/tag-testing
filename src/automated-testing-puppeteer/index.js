const testingHelpers = require('./helpers');

const {
    gaTests,
    dlTests,
    seoTests,
 } = require('./tests');

const args = process.argv.slice(2);
const testType = args[0];

switch(testType) {
    case 'tags':
        //////////////
        // Clickstream tag tests (3rd party)
        testingHelpers.runTest(
            {
                name: 'Clickstream Tag Test [Homepage, Search, Product, Collection, Product, Add to Cart]',
                steps: [
                    // Homepage
                    {
                        "action": "goto",
                        "value": "http://pwa.1800flowers.localhost:3000",
                    },
                    ...testingHelpers.getPageTests(
                        [
                            gaTests,
                            dlTests,
                        ], 'homepage',
                    ),
                    // Search
                    // Product
                    // Collection
                    // Product
                    // Add-To-Cart
                ],
            }, {
                trackRequests: [{
                        name: 'GoogleAnalytics',
                        url: 'www.google-analytics.com/collect',
                        abortRequest: true,
                    },
                    {
                        name: 'Facebook',
                        url: 'www.facebook.com/tr',
                        abortRequest: true,
                    },
                ],
            }
        ).catch((e) => {
            return testingHelpers.log('>>>>>>> TAG TESTS FAILED', 'error');
        });
        break;
    case 'seo':
        /////////////////
        // SEO tests
        testingHelpers.runTest(
            {
                name: 'SEO Tests',
                steps: [
                    // Homepage
                    {
                        "action": "goto",
                        "type": 'homepage',
                        "value": "https://uat2-pwa.1800flowers.dev",
                    },
                    ...testingHelpers.getPageTests(
                        [
                            seoTests
                        ], 'homepage',
                    ),
                    // Collection
                    {
                        "action": "goto",
                        'type': 'collection',
                        "value": "https://uat2-pwa.1800flowers.dev/anniversary",
                    },
                    ...testingHelpers.getPageTests(
                        [
                            seoTests
                        ], 'category',
                    ),
                    // Product
                    {
                        "action": "goto",
                        'type': 'product',
                        "value": "https://uat2-pwa.1800flowers.dev/florist-delivered-floral-embrace-167891",
                    },
                    ...testingHelpers.getPageTests(
                        [
                            seoTests
                        ], 'product',
                    ),
                    // Content
                    {
                        "action": "goto",
                        'type': 'content',
                        "value": "https://uat2-pwa.1800flowers.dev/light-after-loss",
                    },
                    ...testingHelpers.getPageTests(
                        [
                            seoTests
                        ], 'product',
                    ),
                ],
            },
            {
                userAgent: 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            },
        ).catch((e) => {
            return testingHelpers.log('>>>>>>> SEO TESTS FAILED', 'error');
        });
        break;
    default:
        console.log('usage: node automated-testing-puppeteer <testType>');
        break;
}