const chalk = require('chalk');
const { chromium } = require('playwright');
const { URL } = require('url');

const failedTests = [];

const log = (msg, color) => {
    msgColor = chalk.white; // Default

    if (color) {
        switch (color) {
            case "success":
                msgColor = chalk.keyword("green"); // .bgKeyword("limegreen");
                break;
            case "info":
                msgColor = chalk.keyword("dodgerblue"); // .bgKeyword("turqoise");
                break;
            case "error":
                msgColor = chalk.keyword('red'); // .bgKeyword('black');
                break;
            // case "start":    color = "OliveDrab";  bgc = "PaleGreen";       break;
            // case "warning":  color = "Tomato";     bgc = "Black";           break;
            // case "end":      color = "Orchid";     bgc = "MediumVioletRed"; break;
            default: msgColor = color;
        }
    }

    if (Array.isArray(msg)) {
        const [test, result] = msg;
        console.log(`${test}: ${msgColor(result)}`);
    } else if (typeof msg == "object") {
        console.log(msg);
    } else {
        console.log(msgColor(msg));
    }
}
exports.log = log;

const displayTestResults = ({
    name,
    result,
}) => {
    // Capture full list of failed tests
    if (!result) {
        failedTests.push({
            name,
        })
    }

    log([
        name,
        !result ? "FAIL" : "PASS"
    ], !result ? "error" : "success");
};

exports.getPageTests = (testSteps, pageType) => {
    const activeTests = [];

    testSteps.forEach(tests => tests[pageType] && activeTests.push(...tests[pageType]));

    return activeTests;
};

exports.runTest = (testObject, options = { headless: true }) => {
    /*
     * Run a single test from a browser and test sequence
     *
     */
    return new Promise(async(resolve, reject) => {
        const browser = await chromium.launch({
            ...options,
            // _videosPath: __dirname  //  save videos here.
        });

        const context = await browser.newContext({
            // _recordVideos: { width: 1024, height: 768 },  // downscale
            userAgent: options.userAgent,
        });

        // Open new page in browser for the test to run
        const page = await context.newPage();
        // const video = await page.waitForEvent('_videostarted');

        let networkRequests = {};
        if (options.trackRequests) {
            // Turn on request interception to track hits and parameters for e.g. Google Analytics
            // await page.setRequestInterception(true);

            // Create an empty array for each domain we want to track to push to
            options.trackRequests.forEach(tracker => {
                networkRequests[tracker.name] = [];
            });

            const requestUrlParamsToJSON = requestURL => {
                const url = new URL(requestURL);

                return url && url.searchParams ? url.searchParams : {};
            };

            // Log and continue all network requests
            page.route('**', route => {
                // Determine what to do with every request and create an object with the request params when needed
                const request = route.request();
                const requestURL = request.url();
                let abortRequest = false;

                options.trackRequests.forEach(tracker => {
                    if (requestURL.indexOf(tracker.url) > -1) {
                        networkRequests[tracker.name].push({
                            url: requestURL,
                            searchParams: requestUrlParamsToJSON(requestURL),
                        });

                        if (tracker.abortRequest) {
                            abortRequest = true;
                        }
                    }
                });

                return abortRequest ? route.abort() : route.continue();
            });
        }

        // Execute the steps you want to take e.g. go to page, click element, type text, wait, etc. one by one
        try {
            log('-- with options');
            log(options);

            // Go over every step one by one
            for (let i = 0; i < testObject.steps.length; i++) {
                let step = testObject.steps[i];

                // Execute the right step action
                switch (step.action) {
                    case "goto":
                        log(`### Go to page: ${step.value} [${step.type}]`);
                        await page.goto(step.value, { waitUntil: 'networkidle0' });
                        await page.waitForTimeout(1000);
                        break;

                    case "click":
                        log("### Click element: " + step.element);

                        // Wait for the element to appear on screen (useful for asynchronicity)
                        await page.waitForSelector(step.element);

                        // Use page.evaluate because it's more consisten than page.click()
                        await page.evaluate((e) => {
                            document.querySelector(e).click();
                        }, step.element);
                        await page.waitForTimeout(1000);
                        break;

                    case "wait":
                        await page.waitForSelector(step.value);
                        log("### Waiting for: " + step.value);
                        break;

                    case "type":
                        await page.waitForSelector(step.element);
                        if (step.clear) {
                            await page.evaluate((e) => {
                                document.querySelector(e).value = ""
                            }, step.element);
                        }
                        await page.type(step.element, step.value, { delay: 200 });
                        log("### Typing '" + step.value + "' - on DOM element: " + step.element);
                        break;

                    case "test":
                        let result = false;
                        // TEST A: check whether a request parameter matches a value
                        if (step.test.type == "requestMatchRegex") {
                            // Test for request to regex match a value
                            try {
                                // Look at all request objects in the array or only the last one
                                const requests =
                                    (step.test.options.matchAnyRequest == true)
                                        ? networkRequests[step.test.for]
                                        : [networkRequests[step.test.for][networkRequests[step.test.for].length - 1]];

                                const matchRegex = requests.map(request => {
                                    const {
                                        searchParams, // URLSearchParams
                                    } = request;

                                    // If the parameter exists, test for a matching value
                                    if (searchParams.get(step.test.match.key)) {
                                        const regex = new RegExp(step.test.match.value);

                                        if (searchParams.get(step.test.match.key).match(regex) != null) {
                                            return true;
                                        }
                                    }
                                });

                                result = matchRegex.indexOf(true) > -1;

                                displayTestResults({
                                    name: step.test.name,
                                    result,
                                })

                            } catch (e) {
                                reject(e);
                            }
                        }

                        // TEST B: check whether a dataLayer event key matches a specific value
                        if (step.test.type == "matchDataLayerKeyValue") {
                            try {
                                // Get the current dataLayer
                                const pageDataLayer = await page.evaluate(() => window.mbpDataLayerView);

                                // Check for existence of a key / value pair in the datalayer
                                // const result = await pageDataLayer.find(x => x[step.test.key] == step.test.value);
                                // result = (pageDataLayer && typeof pageDataLayer[step.test.key] !== 'undefined') ? (pageDataLayer[step.test.key] === step.test.value) : undefined;
                                result = await (pageDataLayer && pageDataLayer[step.test.key]) ? true : false;

                                displayTestResults({
                                    name: step.test.name,
                                    result,
                                });
                            } catch (e) {
                                reject(e);
                            }
                        }

                        // TEST C: check whether a SEO element is present
                        if (step.test.type == "matchSEOElement") {
                            try {
                                if (step.test.element) {
                                    const {
                                        tag,
                                        attribute,
                                        value,
                                    } = step.test.element;

                                    // Tag
                                    if (tag && !attribute) {
                                        result = await page.$$(`${tag}`);
                                    }

                                    // Attribute
                                    if (tag && attribute) {
                                        result = await page.$$(`${tag}[${attribute}]`);
                                    }

                                    // Attribute and Value
                                    if (tag && attribute && value) {
                                        result = await page.$$(`${tag}[${attribute}='${value}']`);
                                    }
                                }

                                displayTestResults({
                                    name: step.test.name,
                                    result: result.length,
                                });
                            } catch (e) {
                                reject(e);
                            }
                        }

                        break;

                    default:
                        log("This step is not recognised, please use a valid step in your test.", 'error');
                }
            };
        } catch (e) {
            // Make sure the browser is closed, even on time-outs
            browser.close();

            reject(e);
        }

        browser.close();

        // Reject promise is any tests failed
        if (failedTests.length) {
            reject(failedTests);
        }

        // Success
        resolve('SUCCESS');
    });
};
